package com.example.pageobject;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pageobjects.Page;
import service.POService;

import java.lang.reflect.InvocationTargetException;

@SpringBootApplication
public class PageobjectApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(PageobjectApplication.class, args);
	}

	@Override
	public void run(String... args) throws ClassNotFoundException,
			InvocationTargetException,
			NoSuchMethodException,
			InstantiationException,
			IllegalAccessException {

		POService poService = new POService();

		String testValue = "new updated value";
		String maskName = "A001";
		String fieldName = "programmStNr_1";

		Page page = poService.getPageObjectDynamically(maskName);

		//set data dynamically
		page.setField(fieldName, testValue);
		//get data dynamically
		System.out.println(page.getField(fieldName).getValue());

	}

}
