package service;

import pageobjects.Page;

import java.lang.reflect.InvocationTargetException;

public class POService {

    private final static String PACKAGE_SOURCE = "pageobjects";

    public Class<?> getDynamicallyClass(String maskName) throws ClassNotFoundException {
        String className = PACKAGE_SOURCE + "." + maskName;
        Class<?> clazz = Class.forName(className);
        return clazz;
    }

    public Page getPageObjectDynamically(String maskName) throws ClassNotFoundException,
            NoSuchMethodException,
            InvocationTargetException,
            InstantiationException,
            IllegalAccessException {
        Class<?> clazz = getDynamicallyClass(maskName);
        return (Page) clazz.getDeclaredConstructor().newInstance();
    }

}
