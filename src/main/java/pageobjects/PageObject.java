package pageobjects;

public class PageObject {

    private String fieldName;
    private int x;
    private int y;
    private int length;
    private String value;

    PageObject(String fieldName, int x, int y, int length, String value) {
        this.fieldName = fieldName;
        this.x = x;
        this.y = y;
        this.length = length;
        this.value = value;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
