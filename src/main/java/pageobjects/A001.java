package pageobjects;

public class A001 extends Page {

    private PageObject programmStNr_1 = new PageObject("programmStNr_1", 3, 17, 1, "name");
    private PageObject Nl_1 = new PageObject("Nl_1", 3, 21, 2, "name");
    private PageObject Plz_1 = new PageObject("Plz_1", 3, 26, 3, "name");
    private PageObject LfdNr_1 = new PageObject("LfdNr_1", 3, 32, 4, "name");
    private PageObject pz_1 = new PageObject("pz_1", 3, 39, 1, "name");

    private PageObject programmStNr_2 = new PageObject("programmStNr_2", 5, 17, 1, "name");
    private PageObject Nl_2 = new PageObject("Nl_2", 5, 21, 2, "name");
    private PageObject Plz_2 = new PageObject("Plz_2", 5, 26, 3, "name");
    private PageObject LfdNr_2 = new PageObject("LfdNr_2", 5, 32, 4, "name");
    private PageObject pz_2 = new PageObject("pz_2", 5, 39, 1, "name");

    private PageObject hilfsNrJN = new PageObject("hilfsNrJN", 5, 59, 1, "name");

    private PageObject meldung_1 = new PageObject("meldung_1", 24, 2, 22, "name");
    private PageObject meldung_2 = new PageObject("meldung_2", 27, 2, 22, "name");
    private PageObject meldung_3 = new PageObject("meldung_3", 53, 2, 22, "name");

    public PageObject getProgrammStNr_1() {
        return programmStNr_1;
    }

    public PageObject getNl_1() {
        return Nl_1;
    }

    public PageObject getPlz_1() {
        return Plz_1;
    }

    public PageObject getLfdNr_1() {
        return LfdNr_1;
    }

    public PageObject getPz_1() {
        return pz_1;
    }

    public PageObject getProgrammStNr_2() {
        return programmStNr_2;
    }

    public PageObject getNl_2() {
        return Nl_2;
    }

    public PageObject getPlz_2() {
        return Plz_2;
    }

    public PageObject getLfdNr_2() {
        return LfdNr_2;
    }

    public PageObject getPz_2() {
        return pz_2;
    }

    public PageObject getHilfsNrJN() {
        return hilfsNrJN;
    }

    public PageObject getMeldung_1() {
        return meldung_1;
    }

    public PageObject getMeldung_2() {
        return meldung_2;
    }

    public PageObject getMeldung_3() {
        return meldung_3;
    }

    public void setProgrammStNr_1(PageObject programmStNr_1) {
        this.programmStNr_1 = programmStNr_1;
    }

    public void setNl_1(PageObject nl_1) {
        Nl_1 = nl_1;
    }

    public void setPlz_1(PageObject plz_1) {
        Plz_1 = plz_1;
    }

    public void setLfdNr_1(PageObject lfdNr_1) {
        LfdNr_1 = lfdNr_1;
    }

    public void setPz_1(PageObject pz_1) {
        this.pz_1 = pz_1;
    }

    public void setProgrammStNr_2(PageObject programmStNr_2) {
        this.programmStNr_2 = programmStNr_2;
    }

    public void setNl_2(PageObject nl_2) {
        Nl_2 = nl_2;
    }

    public void setPlz_2(PageObject plz_2) {
        Plz_2 = plz_2;
    }

    public void setLfdNr_2(PageObject lfdNr_2) {
        LfdNr_2 = lfdNr_2;
    }

    public void setPz_2(PageObject pz_2) {
        this.pz_2 = pz_2;
    }

    public void setHilfsNrJN(PageObject hilfsNrJN) {
        this.hilfsNrJN = hilfsNrJN;
    }

    public void setMeldung_1(PageObject meldung_1) {
        this.meldung_1 = meldung_1;
    }

    public void setMeldung_2(PageObject meldung_2) {
        this.meldung_2 = meldung_2;
    }

    public void setMeldung_3(PageObject meldung_3) {
        this.meldung_3 = meldung_3;
    }
}
