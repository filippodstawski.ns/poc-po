package pageobjects;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class Page {

    public void setField(String fieldName, String value) {
        try {
            fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            Class<?> clazz = this.getClass();
            PageObject pageObject;
            Method method = clazz.getDeclaredMethod("get" + fieldName);
            try {
                pageObject = (PageObject) method.invoke(this);
                pageObject.setValue(value);
                Method methodUpdate = clazz.getDeclaredMethod("set" + fieldName, PageObject.class);
                methodUpdate.invoke(this, pageObject);
            }
            catch (InvocationTargetException e)
            {
                System.out.println(e.getCause());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PageObject getField(String fieldName) {
        try {
            fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            Class<?> clazz = this.getClass();
            Method method = clazz.getDeclaredMethod("get" + fieldName);
            try {
                return (PageObject) method.invoke(this);
            }
            catch (InvocationTargetException e)
            {
                System.out.println(e.getCause());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
